import React, { Component } from 'react';
import { Button, Modal, ScrollView, View, StyleSheet } from 'react-native';
import Text from 'react-native-elements/src/text/Text';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Algorithm } from '../algorithm/algorithm';
import PrepareButton from '../components/gen-btn/PrepareButton';
import OptionsListContainer from '../components/options-list/OptionsList.container';
import { questions } from '../data/data';
import * as Actions from '../redux/actions/actions';

class NewTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      submitted: false
    };
    console.log('def', props.store.default)
  }

  canGenList() {
    const store = this.props.store.default;
    //must be equal to textOptions length
    const txtOpt = Object.values(store.textOptions).length === 2;
    const listOpt = Object.values(store.listItems).length === questions.length &&
      Object.values(store.listItems).every(val => val.length > 0);

    // save info to store that list can be generated
    this.props.actions.setCanGenList(txtOpt && listOpt);

    return (txtOpt && listOpt);
  }

  handlePrepClick() {
    this.setState({submitted: true});
    if (this.canGenList()) {
      this.algorithm = new Algorithm(this.props);
      this.algorithm.calculate();
    }
  }

  startFresh() {
    this.props.actions.resetApp();
    this.setState({submitted: false});
    console.log('start Fresh clicked', this.props.store);
  }
  showState(){
    console.log(this.props.store)
  }
  showModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }
  render() {
    return (
      <ScrollView flex={1} keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps={'never'}>
        <Modal transparent={true} blurRadius={12}
          visible={this.state.modalVisible}
          animationType={'slide'}
          onRequestClose={() => this.closeModal()}
        >
          <ScrollView style={styles.modalContainer}>
            <View style={styles.innerContainer}>
              <View style={styles.inputPanel}>
                <Text selectable={true}>{JSON.stringify(this.props.store.default)}</Text>
              </View>
              <View style={styles.btnRow}>
                <Button style={{marginTop: 50}}
                        onPress={() => this.closeModal()}
                        title="Close modal"/>
              </View>

            </View>
          </ScrollView>
        </Modal>
        {
          this.debug &&
          <PrepareButton onClick={() => this.handlePrepClick()}
                         modal={() => this.showModal()}
                         onReset={()=>this.startFresh()}/>
        }
        {
          questions.map((questionObj, index) => {
            return (<OptionsListContainer data={questionObj} key={index}
                                          submitted={this.state.submitted}/>)
          })
        }
        <PrepareButton onClick={() => this.handlePrepClick()}
                       modal={() => this.showModal()}
                       onReset={()=>this.startFresh()}/>
        {this.debug && <Button title="Show State" onPress={()=>this.showState()}/>}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: 'rgba(155,155,155,0.6)',
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 50,
  },
  btnRow: {
    marginTop: 50,
    padding: 20,
  },
  inputPanel: {
    padding: 20,
    backgroundColor: 'rgba(255,255,255,0.9)'
  }
});

function mapStateToProps(state, ownProps) {
  return {store: state}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewTrip);
