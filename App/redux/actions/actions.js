import * as types from './actionTypes';


export function login() {
  return {
    type: types.LOGIN
  };
}

export function capture() {
  console.log("capture")
  return {
    type: types.CAPTURE
  };

}

export function close() {
  console.log("close")
  return {
    type: types.CLOSE
  };

}

export function selectItem(itemKey) {
  console.log(`Selecting item of key: ${itemKey}`);
  return {
    type: types.SELECT,
    key: itemKey
  }
}

export function addItemToList(list, itemKey) {
  console.log(`Adding ${itemKey} to list ${list}`);
  return {
    type: types.LIST_ADD,
    list: list,
    key: itemKey
  }
}

export function removeItemFromList(list, itemKey) {
  console.log(`Removing ${itemKey} from list ${list}`);
  return {
    type: types.LIST_REMOVE,
    list: list,
    key: itemKey
  }
}

export function emptyList(list, itemKey) {
  console.log(`Emptying list ${list}`);
  return {
    type: types.LIST_EMPTY,
    list: list,
    key: itemKey
  }
}

export function optionUpdate(option, value) {
  console.log(`Text Input ${option} ${value}`);
  return {
    type: types.OPTION_UPDATE,
    option: option,
    key: value
  }
}

export function setList(list) {
  console.log(`Algorithm setList ${list}`);
  return {
    type: types.SET_LIST,
    list: list
  }
}

export function clearList(list) {
  console.log(`Algorithm clearList ${list}`);
  return {
    type: types.CLEAR_LIST
  }
}


export function setCanGenList(status) {
  console.log(`setCanGenList ${status}`);
  return status ? {
    type: types.CAN_GEN_LIST,
  } : {
    type: types.CANT_GEN_LIST,
  };

}


export function resetApp() {
  console.log(`resetingApp`);
  return {
    type: types.RESET_APP,
  };

}

