import * as types from '../actions/actionTypes';

const defaultStore = {
  accommodation: ["house"],
  genders: ["female"],
  seasons: ["spring"],
  transport: ["plane"],
  tripActivities: ["skiing"],
  tripDestinations: ["mountains"],
  weatherConditions: ["snow"]
};

export default function listItems(state = {}, action) {
  if(action.type === types.LIST_EMPTY || action.type === types.LIST_ADD || action.type === types.LIST_REMOVE){
    const list = state[action.list] ? state[action.list] : state[action.list] = [];
    let index = list ? list.findIndex((e) => e === action.key) : -1;
    switch (action.type) {
      case types.LIST_ADD:
        if (index > -1) {
          return {...state};
        } else {
          return {
            ...state,
            [action.list]: [...list, action.key]
          };
        }

      case types.LIST_REMOVE:
        if (index > -1) {
          return {
            ...state,
            [action.list]: [
              ...list.slice(0, index),
              ...list.slice(index + 1)]
          };
        } else {
          return {...state};
        }

      case types.LIST_EMPTY:
        return {
          ...state,
          [action.list]: []
        };

      default:
        if (state.hasOwnProperty('undefined')) {
          console.log('listitems.js state has undef');
          const {undefined, ...withoutFirst  } = state;
          return withoutFirst;
        } else {
          return state
        }
    }
  }else {
    return {...state};
  }
}