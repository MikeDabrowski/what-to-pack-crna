import * as types from '../actions/actionTypes';

const defaultStore = {
  age: '18',
  days: '7'
};

export default function textOptions(state = {}, action) {
  const option = state[action.option] ? state[action.option] : state[action.option] = '';
  // let index = list ? list.findIndex((e) => e === action.key) : -1;
  switch (action.type) {
    case types.OPTION_UPDATE:
        return {
          ...state,
          [action.option]: action.key
        };

    default:
      if (state.hasOwnProperty('undefined')) {
        console.log('txtOptions.js state has undef');
        const {undefined, ...withoutFirst  } = state;
        return withoutFirst;
      } else {
        return state
      }
  }
}