export const genders = {
  type: 'singleSelect',
  name: 'genders',
  question: 'Kim jesteś?',
  values: {
    female: {id: 'female', name: '', iconSet: 'FontAwesome', iconName: 'female'},
    male: {id: 'male', name: '', iconSet: 'FontAwesome', iconName: 'male'}
  }
};

export const transport = {
  type: 'multiSelect',
  name: 'transport',
  question: 'Czym będziesz podróżować?',
  values: {
    plane: {id: 'plane', name: 'Samolot', iconSet: 'materialCommunityIcons', iconName: 'airplane'},
    coach: {id: 'coach', name: 'Autokar', iconSet: 'materialCommunityIcons', iconName: 'bus'},
    train: {id: 'train', name: 'Pociąg', iconSet: 'materialCommunityIcons', iconName: 'train'},
    car: {id: 'car', name: 'Samochód', iconSet: 'materialCommunityIcons', iconName: 'car'},
    motor: {id: 'motor', name: 'Motocykl', iconSet: 'fontAwesome', iconName: 'motorcycle'},
    hitchhike: {id: 'hitchhike', name: 'Autostop', iconSet: 'fontAwesome', iconName: 'thumbs-up'}
  }
};

export const accommodation = {
  type: 'multiSelect',
  name: 'accommodation',
  question: 'Gdzie się zatrzymasz?',
  values: {
    hotel: {id: 'hotel', name: 'Hotel', iconSet: 'fontAwesome', iconName: 'building-o'},
    house: {id: 'house', name: 'Dom', iconSet: 'fontAwesome', iconName: 'home'},
    tent: {id: 'tent', name: 'Namiot', iconSet: 'materialCommunityIcons', iconName: 'tent'},
  }
};

export const tripActivities = {
  type: 'multiSelect',
  name: 'tripActivities',
  question: 'Co zamierzasz robić?',
  values: {
    hiking: {id: 'hiking', name: 'Wędrowanie', iconSet: 'materialCommunityIcons', iconName: 'walk'},
    skiing: {id: 'skiing', name: 'Sporty zimowe', iconSet: 'custom', iconName: 'skiicon'},
    swimming: {id: 'swimming', name: 'Pywanie', iconSet: 'materialCommunityIcons', iconName: 'swim'},
    cycling: {id: 'cycling', name: 'Kolarstwo', iconSet: 'materialCommunityIcons', iconName: 'bike'},
    angling: {id: 'angling', name: 'Wędkarstwo', iconSet: 'materialCommunityIcons', iconName: 'fish'},
    beach: {id: 'beach', name: 'Plażowanie', iconSet: 'materialCommunityIcons', iconName: 'beach'},
    visiting: {id: 'visiting', name: 'Zwiedzanie', iconSet: 'materialCommunityIcons', iconName: 'binoculars'},
    business: {id: 'business', name: 'Biznes', iconSet: 'fontAwesome', iconName: 'suitcase'}
  }
};

export const weatherConditions = {
  type: 'multiSelect',
  name: 'weatherConditions',
  question: 'Jaką pogodę przewidujesz?',
  values: {
    rain: {id: 'rain', name: 'Deszcz', iconSet: 'ionicons', iconName: 'ios-rainy'},
    snow: {id: 'snow', name: 'Śnieg', iconSet: 'ionicons', iconName: 'ios-snow'},
    sunny: {id: 'sunny', name: 'Słońce', iconSet: 'ionicons', iconName: 'ios-sunny'},
    windy: {id: 'windy', name: 'Wiatr', iconSet: 'materialCommunityIcons', iconName: 'weather-windy'},
  }
};

export const seasons = {
  type: 'multiSelect',
  name: 'seasons',
  question: 'W jaką porę roku jedziesz?',
  values: {
    winter: {id: 'winter', name: 'Zima', iconSet: 'ionicons', iconName: 'ios-snow'},
    spring: {id: 'spring', name: 'Wiosna', iconSet: 'ionicons', iconName: 'ios-flower'},
    summer: {id: 'summer', name: 'Lato', iconSet: 'ionicons', iconName: 'ios-sunny'},
    autumn: {id: 'autumn', name: 'Jesień', iconSet: 'ionicons', iconName: 'ios-leaf'}
  }
};

export const tripDestinations = {
  type: 'multiSelect',
  name: 'tripDestinations',
  question: 'Gdzie planujesz jechać?',
  values: {
    sea: {id: 'sea', name: 'Plaża', iconSet: 'custom', iconName: 'beach'},
    mountains: {id: 'mountains', name: 'Góry', iconSet: 'foundation', iconName: 'mountains'},
    lake: {id: 'lake', name: 'Jezioro', iconSet: 'custom', iconName: 'lake'},
    city: {id: 'city', name: 'Miasto', iconSet: 'materialCommunityIcons', iconName: 'city'}
  }
};

export const questions = [
  genders,
  transport,
  accommodation,
  tripDestinations,
  weatherConditions,
  seasons,
  tripActivities
];

export const documents = [
  {
    id: 'personalId',
    name: 'Dowód osobisty',
    type: 'basic',
    category: 'Dokumenty',
    ageOver: 18
  },
  {
    id: 'insurance',
    name: 'Ubezpieczenie',
    type: 'basic',
    category: 'Dokumenty'
  },
  {
    id: 'creditCard',
    name: 'Karta pałatnicza',
    type: 'basic',
    category: 'Dokumenty'
  },
  {
    id: 'driversLicence',
    name: 'Prawo jazdy',
    type: 'proposed',
    category: 'Dokumenty',
    ageOver: 16
  },
  {
    id: 'money',
    name: 'Pieniądze',
    type: 'basic',
    category: 'Dokumenty'
  },
  {
    id: 'keys',
    name: 'Klucze',
    type: 'basic',
    category: 'Dokumenty'
  },
  {
    id: 'isec',
    name: 'Legitymacja/ISEC',
    type: 'basic',
    category: 'Dokumenty',
    ageUnder: 26
  },
  {
    id: 'planeTickets',
    name: 'Bilety na samolot',
    category: 'Dokumenty',
    transport: [transport.values.plane.id]
  },
  {
    id: 'trainTickets',
    name: 'Bilety na pociąg',
    category: 'Dokumenty',
    transport: [transport.values.train.id]
  },
  {
    id: 'coachTickets',
    name: 'Bilety na autokar',
    category: 'Dokumenty',
    transport: [transport.values.coach.id]
  },
  {
    id: 'carInsurance',
    name: 'Dowód rejestracyjny + OC',
    category: 'Dokumenty',
    transport: [transport.values.car.id]
  },
  {
    id: 'hotelReservation',
    name: 'Potwierdzenie rezerwacji hotelu',
    category: 'Dokumenty',
    accommodation: [accommodation.values.hotel.id]
  },
  {
    id: 'passport',
    name: 'Paszport',
    category: 'Dokumenty',
    transport: [transport.values.plane.id]
  }
];

export const business = [
  {
    id: 'notebook',
    name: 'Laptop + zasilacz',
    category: 'Biznes'
  },
  {
    id: 'usbMemory',
    name: 'Pamięc USB / dysk',
    category: 'Biznes'
  },
  {
    id: 'documents',
    name: 'Dokumenty',
    category: 'Biznes'
  },
  {
    id: 'notepad',
    name: 'Notes / kalendarz',
    category: 'Biznes'
  },
  {
    id: 'presentationAdapter',
    name: 'Przejściówka do prezentacji',
    category: 'Biznes'
  }
];

export const underwear = [
  {
    id: 'pants',
    name: 'Majtki',
    type: 'basic',
    category: 'Bielizna'
  },
  {
    id: 'socks',
    name: 'Skarpety',
    type: 'basic',
    category: 'Bielizna'
  },
  {
    id: 'bra',
    name: 'Biustonosz',
    type: 'basic',
    genders: [genders.values.female.id],
    category: 'Bielizna'
  },
  {
    id: 'sportBra',
    name: 'Biustonosz sportowy',
    type: 'sport',
    genders: [genders.values.female.id],
    category: 'Bielizna'
  },
  {
    id: 'stockings',
    name: 'Rajstopy',
    genders: [genders.values.female.id],
    seasons: [seasons.values.autumn.id, seasons.values.winter.id, seasons.values.spring.id],
    category: 'Bielizna'
  },
  {
    id: 'pajamas',
    name: 'Piżama',
    type: 'basic',
    category: 'Bielizna'
  },
  {
    id: 'sportUnderwear',
    name: 'Bielizna termoaktywna',
    type: 'sport',
    tripActivities: [tripActivities.values.hiking.id, tripActivities.values.skiing.id],
    category: 'Bielizna'
  },
  {
    id: 'swimsuit',
    name: 'Kąpielówki',
    type: 'sport',
    tripActivities: [
      tripActivities.values.swimming.id,
      tripActivities.values.beach.id,
      tripActivities.values.skiing.id
    ],
    tripDestinations: [
      tripDestinations.values.lake.id,
      tripDestinations.values.sea.id
    ],
    category: 'Bielizna'
  }
];

export const clothes = [
  {
    id: 'tShirt',
    name: 'T-shirt',
    type: 'basic',
    category: 'Ubrania'
  },
  {
    id: 'longSleeve',
    name: 'Koszulka z długim rękawem',
    type: 'basic',
    category: 'Ubrania'
  },
  {
    id: 'belt',
    name: 'Pasek',
    type: 'basic',
    category: 'Ubrania'
  },
  {
    id: 'leggins',
    name: 'Legginsy',
    type: 'basic',
    category: 'Ubrania',
    genders: [genders.values.female.id]
  },
  {
    id: 'warmJacket',
    name: 'Ciepła kurtka',
    type: 'basic',
    category: 'Ubrania',
    seasons: [seasons.values.summer.id, seasons.values.spring.id, seasons.values.autumn.id]
  },
  {
    id: 'warmHoodie',
    name: 'Ciepła bluza',
    type: 'basic',
    category: 'Ubrania'
  },
  {
    id: 'scarf',
    name: 'Szalik',
    type: 'basic',
    category: 'Ubrania'
  },
  {
    id: 'neckWarmer',
    name: 'Komin',
    type: 'basic',
    category: 'Ubrania'
  },
  {
    id: 'winterGloves',
    name: 'Rękawice zimowe',
    category: 'Ubrania',
    seasons: [seasons.values.winter.id]
  },
  {
    id: 'fleece',
    name: 'Polar',
    type: 'basic',
    category: 'Ubrania'
  },
  {
    id: 'shortTrousers',
    name: 'Krótkie spodnie',
    type: 'basic',
    category: 'Ubrania',
    seasons: [seasons.values.summer.id, seasons.values.spring.id]
  },
  {
    id: 'businessAttire',
    name: 'Ubranie formalne',
    category: 'Ubrania',
    tripActivities: [tripActivities.values.business.id]
  },
  {
    id: 'trousers',
    name: 'Spodnie',
    type: 'basic',
    category: 'Ubrania'
  },
  {
    id: 'shirt',
    name: 'Koszula',
    type: 'basic',
    category: 'Ubrania'
  }
];

export const shoes = [
  {
    id: 'trainers',
    name: 'Tenisówki',
    category: 'Obuwie'
  },
  {
    id: 'slippers',
    name: 'Klapki / japonki',
    type: 'basic',
    category: 'Obuwie'
  },
  {
    id: 'ankleBoots',
    name: 'Botki',
    category: 'Obuwie',
    genders: [genders.values.female.id],
  },
  {
    id: 'winterBoots',
    name: 'Buty zimowe',
    category: 'Obuwie',
    seasons: [seasons.values.winter.id]
  },
  {
    id: 'elegantShoes',
    name: 'Eleganckie buty',
    category: 'Obuwie',
    tripActivities: [tripActivities.values.business.id]
  }
];

export const cosmetics = [
  {
    id: 'toothBrush',
    name: 'Szczoteczka do zębów',
    type: 'basic',
    category: 'Kosmetyki',
    qty: 1
  },
  {
    id: 'toothPaste',
    name: 'Pasta do zębów',
    type: 'basic',
    category: 'Kosmetyki',
    qty: 1
  },
  {
    id: 'perfumes',
    name: 'Perfumy',
    type: 'basic',
    category: 'Kosmetyki',
    qty: 1
  },
  {
    id: 'tissues',
    name: 'Chusteczki higieniczne',
    type: 'basic',
    category: 'Kosmetyki',
    qty: 1
  },
  {
    id: 'vanityBag',
    name: 'Kosmetyczka',
    type: 'basic',
    category: 'Kosmetyki',
    qty: 1
  },
  {
    id: 'deodorant',
    name: 'Dezodorant',
    type: 'basic',
    category: 'Kosmetyki',
    qty: 1
  },
  {
    id: 'shampoo',
    name: 'Szampon',
    type: 'basic',
    category: 'Kosmetyki',
    qty: 1
  },
  {
    id: 'showerGel',
    name: 'Żel pod prysznic',
    type: 'basic',
    category: 'Kosmetyki',
    qty: 1
  },
  {
    id: 'foundation',
    name: 'Podkład',
    category: 'Kosmetyki',
    genders: [genders.values.female.id],
    qty: 1
  },
  {
    id: 'powder',
    name: 'Puder',
    category: 'Kosmetyki',
    genders: [genders.values.female.id],
    qty: 1
  },
  {
    id: 'lipstick',
    name: 'Szminka',
    category: 'Kosmetyki',
    genders: [genders.values.female.id],
    qty: 1
  }
];

export const firstAidKit = [
  {
    id: 'medicines',
    name: 'Leki',
    type: 'basic',
    category: 'Apteczka',
    qty: 1
  },
  {
    id: 'bandages',
    name: 'Plasterki',
    type: 'basic',
    category: 'Apteczka',
    qty: 1
  },
  {
    id: 'sunburnCream',
    name: 'Krem na oparzenia',
    category: 'Apteczka',
    seasons: [seasons.values.summer, seasons.values.spring],
    qty: 1
  },
  {
    id: 'antiseptic',
    name: 'Środek odkażający',
    category: 'Apteczka',
    qty: 1
  }
];

export const accessories = [
  {
    id: 'mainLuggage',
    name: 'Główny bagaż',
    type: 'basic',
    category: 'Akcesoria',
    qty: 1
  },
  {
    id: 'phone',
    name: 'Telefon',
    type: 'basic',
    category: 'Akcesoria',
    qty: 1
  },
  {
    id: 'headphones',
    name: 'Słuchawki',
    type: 'basic',
    category: 'Akcesoria',
    qty: 1
  },
  {
    id: 'book',
    name: 'Ksiażka',
    type: 'basic',
    category: 'Akcesoria',
    qty: 1
  },
  {
    id: 'phoneCharger',
    name: 'Ładowarka',
    type: 'basic',
    category: 'Akcesoria',
    qty: 1
  },
  {
    id: 'powerBank',
    name: 'PowerBank',
    type: 'basic',
    category: 'Akcesoria',
    qty: 1
  },
  {
    id: 'umbrella',
    name: 'Parasol',
    category: 'Akcesoria',
    weatherConditions: [weatherConditions.values.rain.id]
  },
  {
    id: 'backpack',
    name: 'Plecak',
    category: 'Akcesoria',
    tripActivities: [
      tripActivities.values.beach.id,
      tripActivities.values.hiking.id,
      tripActivities.values.visiting.id,
      tripActivities.values.cycling.id
    ]
  },
  {
    id: 'pocketKnife',
    name: 'Scyzoryk',
    category: 'Akcesoria',
    tripActivities: [
      tripActivities.values.hiking.id,
      tripActivities.values.cycling.id,
      tripActivities.values.angling.id,
    ],
    accommodation: [accommodation.values.tent.id],
    tripDestinations: [tripDestinations.values.lake.id]
  },
  {
    id: 'torch',
    name: 'Latarka',
    category: 'Akcesoria',
    tripActivities: [
      tripActivities.values.angling.id,
      tripActivities.values.cycling.id
    ],
    accommodation: [accommodation.values.tent.id]
  },
  {
    id: 'beachTowel',
    name: 'Ręcznik plażowy',
    category: 'Akcesoria',
    tripActivities: [tripActivities.values.beach.id]
  },
  {
    id: 'sunglasses',
    name: 'Okulary przeciwsłoneczne',
    category: 'Akcesoria'
  },
  {
    id: 'sleepingSack',
    name: 'Śpiwór',
    category: 'Akcesoria',
    accommodation: [accommodation.values.tent]
  },
  {
    id: 'tent',
    name: 'Namiot',
    category: 'Akcesoria',
    accommodation: [accommodation.values.tent]
  },
  {
    id: 'camera',
    name: 'Aparat',
    category: 'Akcesoria',
    tripActivities: [
      tripActivities.values.visiting.id,
      tripActivities.values.beach.id
    ]
  },
  {
    id: 'guide',
    name: 'Przewodnik',
    category: 'Akcesoria',
    tripActivities: [
      tripActivities.values.visiting.id,
    ]
  },
  {
    id: 'map',
    name: 'Mapa',
    category: 'Akcesoria',
    tripActivities: [
      tripActivities.values.visiting.id,
      tripActivities.values.hiking.id,
    ]
  },
  {
    id: 'karrimat',
    name: 'Karimata',
    category: 'Akcesoria',
    accommodation: [accommodation.values.tent]
  },
  {
    id: 'karrimat',
    name: 'Karimata',
    category: 'Akcesoria',
    accommodation: [accommodation.values.tent]
  },
  {
    id: 'lighter',
    name: 'Zapalniczka',
    category: 'Akcesoria',
    accommodation: [accommodation.values.tent]
  },
  {
    id: 'ductTape',
    name: 'Duct Tape',
    category: 'Akcesoria',
    accommodation: [accommodation.values.tent]
  },
  {
    id: 'campingEquipment',
    name: 'Akcesoria biwakowe',
    category: 'Akcesoria',
    accommodation: [accommodation.values.tent]
  }
];

export const motor = [
  {
    id: 'jacket',
    name: 'Kurtka motocyklowa',
    category: 'Motor'
  },
  {
    id: 'protection',
    name: 'Ochraniacze',
    category: 'Motor'
  },
  {
    id: 'helmet',
    name: 'Kask',
    category: 'Motor'
  },
  {
    id: 'spareParts',
    name: 'Części zamienne',
    category: 'Motor'
  },
  {
    id: 'antiTheftProtection',
    name: 'Blokada antykradzieżowa',
    category: 'Motor'
  },
  {
    id: 'cargoBelts',
    name: 'Pasy bagażowe, gumy',
    category: 'Motor'
  },
  {
    id: 'tools',
    name: 'Zestaw naprawczy',
    category: 'Motor'
  },
  {
    id: 'gloves',
    name: 'Rękawice',
    category: 'Motor'
  },
  {
    id: 'documents',
    name: 'Dokumenty',
    category: 'Motor'
  }
];

export const categories = [
  {id: 'documents', name: 'Dokumenty'},
  {id: 'clothes', name: 'Ubrania'},
  {id: 'underwear', name: 'Bielizna'},
  {id: 'accessories', name: 'Akcesoria'},
  {id: 'firstAidKit', name: 'Apteczka'},
  {id: 'cosmetics', name: 'Kosmetyki'},
  {id: 'business', name: 'Biznes'},
  {id: 'motor', name: 'Motor'},
  {id: 'shoes', name: 'Obuwie'}
];


/* must be after everything that goes into Object.assign */
export const items = {
  name: 'items',
  values: [].concat(documents, clothes, underwear, accessories, firstAidKit, cosmetics, motor, business,
    shoes)
};