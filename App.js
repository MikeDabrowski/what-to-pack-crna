import { Font } from 'expo';
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import * as reducers from './App/redux/reducers/index';
import Root from './App/routing/router';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducer = combineReducers(reducers);
export const store = createStoreWithMiddleware(reducer);

export default class App extends Component {
  state = {
    fontLoaded: false,
  };
  async componentWillMount() {
    await Font.loadAsync({
      'fontello': require('./assets/custom-icons/font/fontello.ttf')
    });
    this.setState({ fontLoaded: true });
  }

  render() {
    if(this.state.fontLoaded){
      return (
        <Provider store={store}>
          <Root/>
        </Provider>
      );
    }   else {
      return (
        <View><Text>Wait</Text></View>
      )
    }

  }
}
